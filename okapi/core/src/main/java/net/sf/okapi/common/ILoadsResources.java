package net.sf.okapi.common;

public interface ILoadsResources {
	String getLoadableClassName();

	void setClassLoadingContext(ClassLoader classLoader);
}
